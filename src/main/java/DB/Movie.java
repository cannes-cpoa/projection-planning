package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

public class Movie extends Table {
    @Getter final private String name;

    public Movie(String name) throws NotFoundInTable {
        super("Movie", "name", name);
        this.name = name;
    }

    public Movie(String name, String director, Time duration, Competition competition) throws AlreadyOnTable {
        super("Movie", "name", name, Map.ofEntries(
                Map.entry("name", name),
                Map.entry("director", director),
                Map.entry("duration", duration.toString()),
                Map.entry("CompetitionName", competition.getName())
        ));
        this.name = name;
    }

    public String getDirector() {
        return get(String.class, "director");
    }

    public Time getDuration() {
        return get(Time.class, "duration");
    }

    public Competition getCompetition() {
        return get(Competition.class, "CompetitionName");
    }

    public boolean setDirector(String director) {
        return set("director", director);
    }

    public boolean setDuration(Time duration) {
        return set("duration", duration.toString());
    }

    public boolean setCompetition(Competition competition) {
        return set("CompetitionName", competition.getName());
    }

    static public ArrayList<Movie> getAll() {
        ArrayList<Movie> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM Movie");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Movie(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Movie> getAvailable(Competition competition) {
        ArrayList<Movie> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM Movie WHERE CompetitionName = ?");
            ps.setString(1, competition.getName());
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Movie(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Projection> getProjections() {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE MovieName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public boolean validateDate(LocalDate date) {
        Competition competition = getCompetition();
        int moviesMax = competition.getMovieMax();
        ArrayList<Movie> movies = competition.getTableMovies(Date.valueOf(date));
        return moviesMax <= 0 || movies.stream().filter(m -> m.equals(this)).toArray().length != 0 || movies.size() < moviesMax;
    }

    @Override
    public boolean delete() {
        return false;
    }
}
