package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class Room extends Table {
    @Getter final private String name;

    public Room(String name) throws NotFoundInTable {
        super("Room", "name", name);
        this.name = name;
    }

    public Room(String name, int places) throws AlreadyOnTable {
        super("Room", "name", name, Map.ofEntries(
                Map.entry("name", name),
                Map.entry("places", Integer.toString(places))
        ));
        this.name = name;
    }

    public Integer getPlaces() {
        return get(Integer.class, "places");
    }

    public boolean setPlaces(int places) {
        return set("places", Integer.toString(places));
    }

    static public ArrayList<Room> getAll() {
        ArrayList<Room> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM Room");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Room(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Room> getAll(Movie movie, Slot slot, Date date) {
        ArrayList<Room> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT R.name FROM Room R INNER JOIN CompetitionRoom CR on R.name = CR.RoomName INNER JOIN Projection P on R.name = P.RoomName WHERE CR.CompetitionName = ? AND P.SlotId = ? AND P.startDate = ? GROUP BY R.name");
            ps.setString(1, movie.getCompetition().getName());
            ps.setInt(2, slot.getId());
            ps.setDate(3, date);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Room(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Room> getAvailable(Movie movie, Slot slot, Date date) {
        ArrayList<Room> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT R.name FROM Room R INNER JOIN CompetitionRoom CR on R.name = CR.RoomName LEFT JOIN Projection P on R.name = P.RoomName LEFT JOIN Slot S on S.id = P.SlotId WHERE CR.CompetitionName = ? AND (NOT (S.id = ? AND P.startDate = ?) OR P.id IS NULL) GROUP BY R.name");
            ps.setString(1, movie.getCompetition().getName());
            ps.setInt(2, slot.getId());
            ps.setDate(3, date);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Room(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Competition> getCompetitions() {
        ArrayList<Competition> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT CompetitionName FROM CompetitionRoom WHERE RoomName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Competition(rs.getString("CompetitionName")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Projection> getProjections() {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE RoomName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public boolean addCompetition(Competition competition) {
        return DB.relationSet("CompetitionRoom", "RoomName", this.name, "CompetitionName", competition.getName());
    }

    public boolean removeCompetition(Competition competition) {
        return DB.relationRemove("CompetitionRoom", "RoomName", this.name, "CompetitionName", competition.getName());
    }
}
