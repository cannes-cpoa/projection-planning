package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class User extends Table {
    @Getter final private String email;

    public User(String email) throws NotFoundInTable {
        super("User", "email", email);
        this.email = email;
    }

    public User(String email, String firstName, String lastName, String phoneNumber) throws AlreadyOnTable {
        super("User", "email", email, Map.ofEntries(
                Map.entry("email", email),
                Map.entry("firstName", firstName),
                Map.entry("lastName", lastName),
                Map.entry("phoneNumber", phoneNumber)
        ));
        this.email = email;
    }

    public String getFirstName() {
        return get(String.class, "firstName");
    }

    public String getLastName() {
        return get(String.class, "lastName");
    }

    public String getPhoneNumber() {
        return get(String.class, "phoneNumber");
    }

    public UserType getType() {
        return get(UserType.class, "UserTypeName");
    }

    public Competition getCompetition() {
        return get(Competition.class, "competitionName");
    }

    private String getPasswordHash() {
        return get(String.class, "passwordHash");
    }

    public boolean setFirstName(String firstName) {
        return set("firstName", firstName);
    }

    public boolean setLastName(String lastName) {
        return set("lastName", lastName);
    }

    public boolean setPhoneNumber(String phoneNumber) {
        return set("phoneNumber", phoneNumber);
    }

    public boolean setType(UserType userType) {
        return set("UserTypeName", userType.getName());
    }

    public boolean setCompetition(Competition competition) {
        return set("CompetitionName", competition.getName());
    }

    public String checkPassword() {
        return null;
    }

    public boolean setPassword(String password) {
        return false;
    }

    static public ArrayList<User> getAll() {
        ArrayList<User> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT email FROM User");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new User(rs.getString("email")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }
}
