package DB;

import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class Projection extends Table {
    @Getter final private int id;

    public Projection(int id) throws NotFoundInTable {
        super("Projection", "id", Integer.toString(id));
        this.id = id;
    }

    public Projection(Date startDate, Competition competition, Room room, Movie movie, Slot slot) {
        super("Projection", "id", Map.ofEntries(
                Map.entry("startDate", startDate.toString()),
                Map.entry("CompetitionName", competition.getName()),
                Map.entry("RoomName", room.getName()),
                Map.entry("MovieName", movie.getName()),
                Map.entry("SlotId", Integer.toString(slot.getId()))
        ));
        this.id = Integer.parseInt(super.checkValue);
    }

    public Date getStartDate() {
        return get(Date.class, "startDate");
    }

    public Competition getCompetition() {
        return get(Competition.class, "CompetitionName");
    }

    public Room getRoom() {
        return get(Room.class, "RoomName");
    }

    public Movie getMovie() {
        return get(Movie.class, "MovieName");
    }

    public Slot getSlot() {
        return get(Slot.class, "SlotId");
    }

    public boolean setStartDate(Date startDate) {
        return set("startDate", startDate.toString());
    }

    public boolean setCompetition(Competition competition) {
        return set("CompetitionName", competition.getName());
    }

    public boolean setRoom(Room room) {
        return set("RoomName", room.getName());
    }

    public boolean setMovie(Movie movie) {
        return set("MovieName", movie.getName());
    }

    static public ArrayList<Projection> getAll() {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Projection> getAvailable(Competition competition) {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE CompetitionName = ?");
            ps.setString(1, competition.getName());
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public Projection find(Date date, Competition competition, Room room, Movie movie, Slot slot) {
        Projection projection = null;
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE startDate = ? AND CompetitionName = ? AND RoomName = ? AND MovieName = ?  AND SlotId = ?");
            ps.setDate(1, date);
            ps.setString(2, competition.getName());
            ps.setString(3, room.getName());
            ps.setString(4, movie.getName());
            ps.setInt(5, slot.getId());
            if (ps.execute()) {
                ResultSet rs = ps.getResultSet();
                rs.next();
                projection = new Projection(rs.getInt("id"));
            }
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return projection;
    }

    @Override
    public String toString() {
        return getMovie().getName();
    }

    public String[] toArray() {
        return new String[]{getMovie().toString(), getCompetition().toString(), getRoom().toString(), getStartDate().toString(), getSlot().toString()};
    }
}
