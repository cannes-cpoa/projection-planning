package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;

import java.util.Map;
import java.util.Objects;

public abstract class Table {
    final protected String tableName;
    final protected String check;
    final protected String checkValue;

    protected Table(String tableName, String check, String checkValue) throws NotFoundInTable {
        this.tableName = tableName;
        this.check = check;
        this.checkValue = checkValue;
        if (!exist())
            throw new NotFoundInTable(this.tableName);
    }

    protected Table(String tableName, String check, String checkValue, Map<String, String> values) throws AlreadyOnTable {
        this.tableName = tableName;
        this.check = check;
        this.checkValue = checkValue;
        DB.create(this.getClass(), this.tableName, this.check, values);
    }

    protected Table(String tableName, String check, Map<String, String> values) {
        this.tableName = tableName;
        this.check = check;
        this.checkValue = DB.create(this.getClass(), this.tableName, values);
    }

    protected <T> T get(Class<T> cl, String value) {
        return DB.get(cl, tableName, value, check, checkValue);
    }

    protected boolean set(String value, String newValue) {
        return DB.set(tableName, value, newValue, check, checkValue);
    }

    public boolean exist() {
        return DB.exist(tableName, check, checkValue);
    }

    public boolean delete() {
        return DB.delete(tableName, check, checkValue);
    }

    @Override
    public String toString() {
        return checkValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return tableName.equals(table.tableName) && checkValue.equals(table.checkValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableName, checkValue);
    }
}
