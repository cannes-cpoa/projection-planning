package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * # Competition
 * <p>
 * ## name
 * <p>
 * The event name
 * <p>
 * ## days
 * <p>
 * The number of days a competition is planned
 * <p>
 * ## movies
 * <p>
 * The number of movies per day
 * <p>
 * ## movieMax
 * <p>
 * The maximum number of different movies projected
 */
public class Competition extends Table {
    @Getter final private String name;

    public Competition(String name) throws NotFoundInTable {
        super("Competition", "name", name);
        this.name = name;
    }

    public Competition(String name, int days, int movies, int movieMax) throws AlreadyOnTable {
        super("Competition", "name", name, Map.ofEntries(
                Map.entry("name", name),
                Map.entry("days", Integer.toString(days)),
                Map.entry("movies", Integer.toString(movies)),
                Map.entry("movieMax", Integer.toString(movieMax))
        ));
        this.name = name;
    }

    public Integer getDays() {
        return get(Integer.class, "days");
    }

    public Integer getMovies() {
        return get(Integer.class, "movies");
    }

    public Integer getMovieMax() {
        return get(Integer.class, "movieMax");
    }

    public boolean setDays(int days) {
        return set("days", Integer.toString(days));
    }

    public boolean setMovies(int movies) {
        return set("movies", Integer.toString(movies));
    }

    public boolean setMovieMax(int movieMax) {
        return set("movieMax", Integer.toString(movieMax));
    }

    static public ArrayList<Competition> getAll() {
        ArrayList<Competition> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM Competition");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Competition(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT email FROM User WHERE CompetitionName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new User(rs.getString("email")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Slot> getSlots() {
        ArrayList<Slot> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Slot WHERE CompetitionName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Slot(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Room> getRooms() {
        ArrayList<Room> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT RoomName FROM CompetitionRoom WHERE CompetitionName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Room(rs.getString("RoomName")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Projection> getProjections() {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE CompetitionName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Movie> getTableMovies() {
        ArrayList<Movie> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM Movie WHERE CompetitionName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Movie(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<Movie> getTableMovies(Date date) {
        ArrayList<Movie> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT M.name FROM Movie M INNER JOIN Projection P on M.name = P.MovieName WHERE M.CompetitionName = ? AND P.startDate = ? GROUP BY M.name");
            ps.setString(1, name);
            ps.setDate(2, date);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Movie(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public boolean addRoom(Room room) {
        return DB.relationSet("CompetitionRoom", "CompetitionName", this.name, "RoomName", room.getName());
    }

    public boolean removeRoom(Room room) {
        return DB.relationRemove("CompetitionRoom", "CompetitionName", this.name, "RoomName", room.getName());
    }
}
