package DB;

import Config.Config;
import Exceptions.AlreadyOnTable;
import Exceptions.InvalidConfig;
import Exceptions.NotFoundInTable;
import lombok.Getter;
import org.json.simple.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class DB {
    @Getter static private Connection connection;

    static public boolean connect() {
        try {
            if (connection == null || connection.isClosed()) {
                JSONObject auth = Config.getDBAuth();
                if (auth != null)
                    connection = DriverManager.getConnection("jdbc:mariadb://"+auth.get("host")+"/"+auth.get("database")+"?user="+auth.get("user")+"&password="+auth.get("password"));
                return true;
            }
        } catch (SQLException | InvalidConfig e) {
            return false;
        }
        return false;
    }

    static public boolean disconnect() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    static public boolean isConnected() {
        try {
            if (connection != null && !connection.isClosed())
                return true;
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    static public <T> T get(Class<T> cl, String table, String value, String check, String checkValue) {
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT "+value+" FROM "+table+" WHERE "+check+" = ?");
            ps.setString(1, checkValue);
            if (ps.execute()) {
                ResultSet rs = ps.getResultSet();
                rs.next();
                Object returnValue;
                if (cl == String.class)
                    returnValue = rs.getString(value);
                else if (cl == Integer.class)
                    returnValue = rs.getInt(value);
                else if (cl == Time.class)
                    returnValue = rs.getTime(value);
                else if (cl == Date.class)
                    returnValue = rs.getDate(value);
                else if (cl == Competition.class)
                    returnValue = new Competition(rs.getString(value));
                else if (cl == Movie.class)
                    returnValue = new Movie(rs.getString(value));
                else if (cl == Projection.class)
                    returnValue = new Projection(rs.getInt(value));
                else if (cl == Room.class)
                    returnValue = new Room(rs.getString(value));
                else if (cl == Slot.class)
                    returnValue = new Slot(rs.getInt(value));
                else if (cl == User.class)
                    returnValue = new Slot(rs.getInt(value));
                else if (cl == UserType.class)
                    returnValue = new UserType(rs.getString(value));
                else
                    returnValue = rs.getObject(value);
                ps.close();
                return (T)returnValue;
            }
        } catch (SQLException | NotFoundInTable e) {
            return null;
        }
        return null;
    }

    static public boolean set(String table, String value, String newValue, String check, String checkValue) {
        try {
            PreparedStatement ps = DB.connection.prepareStatement("UPDATE "+table+" SET "+value+" = ? WHERE "+check+" = ?");
            ps.setString(1, newValue);
            ps.setString(2, checkValue);
            int r = ps.executeUpdate();
            ps.close();
            if (r > 0)
                return true;
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    static public boolean relationSet(String table, String valueName1, String value1, String valueName2, String value2) {
        try {
            PreparedStatement ps = DB.connection.prepareStatement("INSERT INTO "+table+" ("+valueName1+", "+valueName2+") VALUES (?, ?)");
            ps.setString(1, value1);
            ps.setString(2, value2);
            int r = ps.executeUpdate();
            ps.close();
            return r > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    static public boolean relationRemove(String table, String valueName1, String value1, String valueName2, String value2) {
        try {
            PreparedStatement ps = DB.connection.prepareStatement("DELETE FROM "+table+" WHERE "+valueName1+" = ? AND "+valueName2+" = ?");
            ps.setString(1, value1);
            ps.setString(2, value2);
            int r = ps.executeUpdate();
            ps.close();
            return r > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    static public boolean exist(String table, String check, String checkValue) {
        try {
            boolean r = false;
            PreparedStatement ps = DB.connection.prepareStatement("SELECT * FROM "+table+" WHERE "+check+" = ?");
            ps.setString(1, checkValue);
            ps.execute();
            if (ps.getResultSet().next())
                r = true;
            ps.close();
            return r;
        } catch (SQLException e) {
            return false;
        }
    }

    static public <T> T create(Class<T> cl, String table, String id, Map<String, String> values) throws AlreadyOnTable {
        try {
            if (exist(table, id, values.get(id)))
                throw new AlreadyOnTable(values.get(id));
            String query = String.format(
                    "INSERT INTO "+table+" (%s) VALUES (%s)",
                    String.join(",", values.keySet()),
                    String.join(",", Collections.nCopies(values.values().size(), "?"))
            );
            PreparedStatement ps = DB.connection.prepareStatement(query);
            int i = 1;
            for (String value : values.values()) {
                ps.setString(i, value);
                i++;
            }
            int r = ps.executeUpdate();
            ps.close();
            if (r > 0)
                return cl.getConstructor(String.class).newInstance(values.get(id));
        } catch (SQLException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
        return null;
    }

    static public String create(Class cl, String table, Map<String, String> values) {
        try {
            String query = String.format(
                    "INSERT INTO "+table+" (%s) VALUES (%s)",
                    String.join(",", values.keySet()),
                    String.join(",", Collections.nCopies(values.values().size(), "?"))
            );
            PreparedStatement ps;
            ps = DB.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for (String value : values.values()) {
                ps.setString(i, value);
                i++;
            }
            ps.execute();
            ResultSet gk = ps.getGeneratedKeys();
            gk.next();
            String rs = gk.getObject(1).toString();
            ps.close();
            return rs;
        } catch (SQLException e) {
            return null;
        }
    }

    static public boolean delete(String table, String check, String checkValue) {
        try {
            PreparedStatement ps = DB.connection.prepareStatement("DELETE FROM "+table+" WHERE "+check+" = ?");
            ps.setString(1, checkValue);
            int r = ps.executeUpdate();
            ps.close();
            if (r > 0)
                return true;
        } catch (SQLException e) {
            return false;
        }
        return false;
    }
}
