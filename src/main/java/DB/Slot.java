package DB;

import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class Slot extends Table {
    @Getter final private int id;

    public Slot(int id) throws NotFoundInTable {
        super("Slot", "id", Integer.toString(id));
        this.id = id;
    }

    public Slot(Date startTime, Competition competition) {
        super("Slot", "id", Map.ofEntries(
                Map.entry("startTime", startTime.toString()),
                Map.entry("CompetitionName", competition.getName())
        ));
        this.id = Integer.parseInt(super.checkValue);
    }

    public Time getStartTime() {
        return get(Time.class, "startTime");
    }

    public Competition getCompetition() {
        return get(Competition.class, "CompetitionName");
    }

    public boolean setCompetition(Competition competition) {
        return set("CompetitionName", competition.getName());
    }

    public boolean setStartTime(Date startTime) {
        return set("startTime", startTime.toString());
    }

    public ArrayList<Projection> getProjections() {
        ArrayList<Projection> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Projection WHERE SlotId = ?");
            ps.setInt(1, id);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Projection(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Slot> getAll() {
        ArrayList<Slot> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Slot");
            for (ResultSet rs = ps.executeQuery(); rs.next(); )
                list.add(new Slot(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Slot> getAll(Competition competition) {
        ArrayList<Slot> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT id FROM Slot WHERE CompetitionName = ? ORDER BY startTime");
            ps.setString(1, competition.getName());
            for (ResultSet rs = ps.executeQuery(); rs.next(); )
                list.add(new Slot(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Slot> getAll(Competition competition, Date date) {
        ArrayList<Slot> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT S.id FROM Slot S INNER JOIN Projection P on S.id = P.SlotId WHERE S.CompetitionName = ? AND P.startDate = ? GROUP BY S.id, S.startTime ORDER BY S.startTime");
            ps.setString(1, competition.getName());
            ps.setDate(2, date);
            for (ResultSet rs = ps.executeQuery(); rs.next(); )
                list.add(new Slot(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    static public ArrayList<Slot> getAvailable(Competition competition, Date date) {
        ArrayList<Slot> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT S.id FROM Slot S LEFT JOIN Projection P ON S.id = P.SlotId WHERE S.CompetitionName = ? AND (P.id IS NULL OR P.startDate != ?) GROUP BY S.id, S.startTime ORDER BY S.startTime");
            ps.setString(1, competition.getName());
            ps.setDate(2, date);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new Slot(rs.getInt("id")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    @Override
    public String toString() {
        return getStartTime().toString();
    }
}
