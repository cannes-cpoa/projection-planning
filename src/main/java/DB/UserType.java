package DB;

import Exceptions.AlreadyOnTable;
import Exceptions.NotFoundInTable;
import lombok.Getter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class UserType extends Table {
    @Getter final private String name;

    public UserType(String name) throws NotFoundInTable {
        super("UserType", "name", name);
        this.name = name;
    }

    static public UserType create(String name) throws AlreadyOnTable {
        return DB.create(UserType.class, "UserType", "name", Map.ofEntries(
                Map.entry("name", name)
        ));
    }

    static public ArrayList<UserType> getAll() {
        ArrayList<UserType> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT name FROM UserType");
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new UserType(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }

    public ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList<>();
        try {
            PreparedStatement ps = DB.getConnection().prepareStatement("SELECT email FROM User WHERE UserTypeName = ?");
            ps.setString(1, name);
            for (ResultSet rs = ps.executeQuery(); rs.next();)
                list.add(new User(rs.getString("name")));
            ps.close();
        } catch (SQLException | NullPointerException | NotFoundInTable e) {
            return null;
        }
        return list;
    }
}
