package Exceptions;

public class InvalidConfig extends Exception {
    public InvalidConfig() {
        super("Configuration file is invalid");
    }
}
