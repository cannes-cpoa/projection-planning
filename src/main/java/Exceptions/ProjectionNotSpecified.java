package Exceptions;

public class ProjectionNotSpecified extends Exception {
    public ProjectionNotSpecified() {
        super("Can't edit without a specified projection !");
    }
}
