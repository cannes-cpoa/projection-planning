package Exceptions;

public class NotFoundInTable extends Exception {
    public NotFoundInTable(String name) {
        super(name+" not found in table");
    }
}
