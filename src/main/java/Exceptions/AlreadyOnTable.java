package Exceptions;public class AlreadyOnTable extends Exception {
    public AlreadyOnTable(String name) {
        super(name+" is already on the table");
    }
}
