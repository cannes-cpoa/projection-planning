package Config;

import Exceptions.InvalidConfig;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

public class Config {
    static private final JSONObject template = new JSONObject(
            Map.ofEntries(
                    Map.entry("db",
                            new JSONObject(Map.ofEntries(Map.entry("host", ""),
                                    Map.entry("database", ""),
                                    Map.entry("user", ""),
                                    Map.entry("password", "")))
                    ),
                    Map.entry("startDate", LocalDate.of(2021, 5, 11).toString()),
                    Map.entry("endDate", LocalDate.of(2021, 5, 22).toString())
            )
    );

    static private void createConfig() {
        File f = new File("./config.json");
        try {
            FileWriter fw = new FileWriter(f);
            fw.write(template.toJSONString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static private boolean checkConfig(JSONObject jsonObject, JSONObject template) {
        for (Object k : template.keySet().toArray())
            if (!jsonObject.containsKey(k) ||
                    (template.get(k).getClass() == JSONObject.class &&
                            !checkConfig((JSONObject) jsonObject.get(k), (JSONObject) template.get(k))))
                return false;
        return true;
    }

    static private JSONObject getConfig() throws InvalidConfig {
        try {
            File f = new File("./config.json");
            if (!f.exists())
                createConfig();
            JSONObject config = (JSONObject) new JSONParser().parse(new FileReader(f));
            if (checkConfig(config, template))
                return config;
            else
                throw new InvalidConfig();
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    static public JSONObject getDBAuth() throws InvalidConfig {
        return (JSONObject) Config.getConfig().get("db");
    }

    static public LocalDate getStartDate() {
        try {
            return LocalDate.parse((String) Config.getConfig().get("startDate"));
        } catch (InvalidConfig e) {
            return null;
        }
    }

    static public LocalDate getEndDate() {
        try {
            return LocalDate.parse((String) Config.getConfig().get("endDate"));
        } catch (InvalidConfig e) {
            return null;
        }
    }
}
