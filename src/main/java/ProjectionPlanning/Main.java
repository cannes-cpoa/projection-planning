package ProjectionPlanning;

import DB.DB;
import Exceptions.NotFoundInTable;
import GUI.GUI;
import mdlaf.MaterialLookAndFeel;
import mdlaf.themes.MaterialLiteTheme;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws NotFoundInTable {
        DB.connect();
        try {
            UIManager.setLookAndFeel(new MaterialLookAndFeel(new MaterialLiteTheme()));
        } catch (UnsupportedLookAndFeelException ignored) {}
        new GUI();
    }
}
