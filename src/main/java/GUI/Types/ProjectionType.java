package GUI.Types;

import lombok.Getter;

public enum ProjectionType {
    ADD("Add projection", "Add"),
    EDIT("Edit projection", "Edit"),
    REMOVE("Remove projection", "Remove");
    @Getter final private String title;
    @Getter final private String text;

    ProjectionType(String title, String text) {
        this.title = title;
        this.text = text;
    }
}