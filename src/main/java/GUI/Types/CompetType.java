package GUI.Types;

import DB.Competition;
import DB.Slot;
import Exceptions.NotFoundInTable;

import java.util.ArrayList;

public enum CompetType {
    LM("Long Métrage"),
    UCR("Un Certain Regard"),
    HC("Hors Compétition");
    final private String competition;

    CompetType(String competition) {
        this.competition = competition;
    }

    public ArrayList<Slot> getSlots() throws NotFoundInTable {
        return Slot.getAll(new Competition(competition));
    }

    public String getCompetition() {
        return this.competition;
    }
}