package GUI.Agenda;

import DB.Projection;
import GUI.Types.ProjectionType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PopUpProjection extends JPopupMenu implements ActionListener {
    JMenuItem itemAdd;
    JMenuItem itemEdit;
    JMenuItem itemRemove;
    Projection projection;
    Agenda agenda;
    public PopUpProjection(Projection projection, Agenda agenda) {
        this.agenda = agenda;
        itemAdd = new JMenuItem("Add");
        itemEdit = new JMenuItem("Edit");
        itemRemove = new JMenuItem("Remove");
        itemAdd.addActionListener(this);
        itemEdit.addActionListener(this);
        itemRemove.addActionListener(this);
        if (projection != null) {
            this.projection = projection;
            add(itemEdit);
            add(itemRemove);
        }
        else {
            this.projection = null;
            add(itemAdd);
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if (source == itemAdd) {
            agenda.openDialog(ProjectionType.ADD);

        }
        else if (source == itemEdit) {
            agenda.openDialog(ProjectionType.EDIT, projection);

        }
        else if (source == itemRemove) {
            agenda.openDialog(ProjectionType.REMOVE, projection);
        }
    }
}
