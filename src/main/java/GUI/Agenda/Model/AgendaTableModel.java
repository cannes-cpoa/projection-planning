package GUI.Agenda.Model;

import Config.Config;
import DB.Projection;

import javax.swing.table.AbstractTableModel;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class AgendaTableModel extends AbstractTableModel {
    private LocalDate[] columnNames;
    private Object[][] data;
    private int day;

    public AgendaTableModel(int day, int currentPage) {
        this.day = day;
        columnNames = new LocalDate[this.day];
        data = new Object[5][this.day];

        LocalDate startDate = Config.getStartDate();
        for (int i = 0; i < this.day; i++) {
            this.columnNames[i] = startDate.plusDays(i + 5 * (currentPage));
        }
        for (int i = 0; i < 5; i++) {
            Arrays.fill(data[i], "");
        }
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    public LocalDate getColumn(int column) {
        return columnNames[column];
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column].toString();
    }

    public String[] getColumnNames() {
        return Arrays.stream(columnNames).map(LocalDate::toString).toArray(String[]::new);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object dataObject = data[rowIndex][columnIndex];
        if (dataObject.getClass() == DB.Projection[].class) {
            ArrayList<Projection> projections = new ArrayList<Projection>() {
                @Override
                public String toString() {
                    StringBuilder builder = new StringBuilder("<html>");
                    for (int i = 0; i < size(); i++) {
                        builder.append(String.format("%s<br>", get(i)));
                    }
                    builder.append("</html>");
                    return builder.toString();
                }
            };
            Projection[] dataArray = (Projection[]) dataObject;
            projections.addAll(Arrays.asList(dataArray));
            return projections;
        }
        return data[rowIndex][columnIndex];
    }

    public void addValueAt(Projection projection, int rowIndex, int columnIndex) {
        Projection[] projections = (Projection[]) data[rowIndex][columnIndex];
        ArrayList<Projection> projectionsArrayList = new ArrayList<>(Arrays.asList(projections)) {
            @Override
            public String toString() {
                StringBuilder builder = new StringBuilder("<html>");
                for (int i = 0; i < size(); i++) {
                    builder.append(String.format("%s<br>", get(i)));
                }
                builder.append("</html>");
                return builder.toString();
            }
        };
        projectionsArrayList.add(projection);
        setValueAt(projectionsArrayList, rowIndex, columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data[rowIndex][columnIndex] = aValue;
    }
}
