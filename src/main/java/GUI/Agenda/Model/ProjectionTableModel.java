package GUI.Agenda.Model;

import DB.Projection;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class ProjectionTableModel extends AbstractTableModel {
    private String[] columnNames = {"Movie", "Competition", "Room", "Start", "Slot"};
    private Projection[] data;

    public ProjectionTableModel(ArrayList<Projection> projections) {
        data = new Projection[projections.size()];
        for (int i = 0; i < projections.size(); i++) {
            data[i] = projections.get(i);
        }
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Projection getRowAt(int row) {
        return data[row];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex].toArray()[columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        data[rowIndex] = (Projection) aValue;
    }
}
