package GUI.Agenda;

import Config.Config;
import DB.Competition;
import DB.Projection;
import DB.Slot;
import Exceptions.ProjectionNotSpecified;
import GUI.Agenda.Model.AgendaTableModel;
import GUI.ProjectionHandler;
import GUI.ProjectionSelecter;
import GUI.Types.ProjectionType;
import lombok.Getter;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Agenda extends JPanel {
    private JPanel agendaPanel;
    private Object headers[];
    private int currentPage;
    private JTable table;
    @Getter
    private Competition competition;
    private Agenda agenda;
    private ArrayList<Slot> slots;
    private Slot currentSlot;
    private LocalDate currentDay;

    public Agenda(JPanel agendaPanel, int currentPage, Competition competition) {
        this.agendaPanel = agendaPanel;
        this.slots = Slot.getAll(competition);
        this.headers = slots.toArray();
        this.currentPage = currentPage;
        this.competition = competition;
        this.agendaPanel.removeAll();
        int totalPages[] = dayToPage();
        if (this.currentPage >= totalPages.length) {
            this.currentPage = totalPages.length - 1;
        } else if (this.currentPage < 0) {
            this.currentPage = 0;
        }
        JScrollPane scroll = constructAgenda(totalPages[this.currentPage]);
        agendaPanel.add(scroll);
    }

    public void refresh() {
        this.agendaPanel.removeAll();
        int totalPages[] = dayToPage();
        if (this.currentPage > totalPages.length) {
            this.currentPage = totalPages.length - 1;
        } else if (this.currentPage < 0) {
            this.currentPage = 0;
        }
        JScrollPane scroll = constructAgenda(totalPages[this.currentPage]);
        agendaPanel.add(scroll);
        agendaPanel.repaint();
        agendaPanel.revalidate();
    }

    private int[] dayToPage() {
        int totalDay = Config.getEndDate().compareTo(Config.getStartDate());
        int day = totalDay;
        int count = 0;
        while (day >= 5) {
            day = day - 5;
            count++;
        }
        int rest = totalDay - 5 * count;
        int totalPages = rest < 5 ? count + 1 : count;
        int pages[] = new int[totalPages];
        Arrays.fill(pages, 5);
        if (rest < 5) pages[totalPages - 1] = rest;
        return pages;
    }
    private JScrollPane constructAgenda(int day) {
        ListModel lm = new AbstractListModel() {
            public int getSize() {
                return headers.length;
            }

            public Object getElementAt(int index) {
                return headers[index];
            }
        };
        AgendaTableModel dm = new AgendaTableModel(day, currentPage);
        table = new JTable(dm) {
            @Override
            public boolean getScrollableTracksViewportHeight() {
                return true;
            }
        };
        table.setCellSelectionEnabled(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(100);
        agenda = this;
        table.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                if (mouseEvent.isPopupTrigger()) {
                    int row = table.rowAtPoint(mouseEvent.getPoint());
                    int column = table.getTableHeader().columnAtPoint(mouseEvent.getPoint());
                    table.changeSelection(row, column, false, false);
                    Object currentCellValue = table.getValueAt(row, column);
                    Projection projection = null;
                    if (currentCellValue.getClass() == Projection.class)
                        projection = (Projection) currentCellValue;
                    PopUpProjection popUpProjection = new PopUpProjection(projection, agenda);
                    popUpProjection.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                int row = table.rowAtPoint(mouseEvent.getPoint());
                int column = table.getTableHeader().columnAtPoint(mouseEvent.getPoint());
                currentDay = dm.getColumn(column);
                currentSlot = (Slot) lm.getElementAt(row);
                Object currentCellValue = table.getValueAt(row, column);
                Projection projection = null;
                if (currentCellValue.getClass() == Projection.class)
                    projection = (Projection) currentCellValue;
                if (mouseEvent.getClickCount() == 2 && table.getSelectedRow() != -1) {
                    if (projection == null && currentCellValue.getClass() != String.class) {
                        ProjectionSelecter dialog = new ProjectionSelecter((ArrayList<Projection>) currentCellValue, agenda);
                        dialog.pack();
                        dialog.setVisible(true);
                    } else if (projection == null)
                        openDialog(ProjectionType.ADD);
                    else openDialog(ProjectionType.EDIT, projection);
                } else if (mouseEvent.isPopupTrigger()) {
                    PopUpProjection popUpProjection = new PopUpProjection(projection, agenda);
                    popUpProjection.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                }
            }
        });

        TableColumnModel columnModel = table.getColumnModel();
        int columnCount = columnModel.getColumnCount();
        for (int i = 0; i < columnCount; i++) {
            table.getColumnModel().getColumn(i).setPreferredWidth(200);
        }


        JList rowHeader = new JList(lm);
        rowHeader.setFixedCellWidth(100);
        rowHeader.setFixedCellHeight(100);

        rowHeader.setCellRenderer(new RowHeaderRenderer(table));

        JScrollPane scroll = new JScrollPane(table);
        scroll.setRowHeaderView(rowHeader);
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        this.agendaPanel.setPreferredSize(new Dimension(this.agendaPanel.getWidth(), headers.length * 100 + 20));
        updateMovies();
        return scroll;
    }

    private void updateMovies() {
            ArrayList<Projection> projections = Projection.getAvailable(competition);
            for (Projection projection : projections) {
                addMovie(projection);

            }
    }

    public void openDialog(ProjectionType projectionType) {
        try {
            ProjectionHandler dialog = new ProjectionHandler(projectionType, this);
            dialog.pack();
            dialog.setVisible(true);
        } catch (ProjectionNotSpecified ignored) {
        }
    }

    public void openDialog(ProjectionType projectionType, Projection projection) {
        ProjectionHandler dialog = new ProjectionHandler(projectionType, this, projection);
        dialog.pack();
        dialog.setVisible(true);
    }

    private void addMovie(Projection projection) {
        Date startDate = projection.getStartDate();
        if (Arrays.asList(((AgendaTableModel) this.table.getModel()).getColumnNames()).contains(startDate.toString())) {
            Time time = projection.getSlot().getStartTime();
            int column = this.table.getColumn(startDate.toString()).getModelIndex();
            int row = 0;
            for (int i = 0; i < slots.size(); i++) {
                if (slots.get(i).getStartTime().toString().equals(time.toString())) {
                    row = i;
                    break;
                }
            }
            Object tableValue = table.getValueAt(row, column);
            if (tableValue.getClass() == Projection.class) {
                Projection[] projections = {(Projection) tableValue, projection};
                table.setValueAt(projections, row, column);
            } else if (tableValue.getClass() != String.class) {
                ((AgendaTableModel) table.getModel()).addValueAt(projection, row, column);
            } else {
                table.setValueAt(projection, row, column);
            }
        }
    }
    public int getCurrentPage() {
        return this.currentPage;
    }


    public Slot getSlot() {
        return currentSlot;
    }

    public LocalDate getDate() {
        return currentDay;
    }
}
