package GUI.Theme;

import mdlaf.animation.MaterialUIMovement;
import mdlaf.components.button.MaterialButtonUI;
import mdlaf.utils.MaterialColors;

import javax.swing.*;

public class PrimaryContainedButtonUI extends MaterialButtonUI {
    @Override
    public void installUI(JComponent c) {
        super.mouseHoverEnabled = false;
        super.installUI(c);
        super.mouseHoverEnabled = true;
        super.colorMouseHoverNormalButton = MaterialColors.BLUE_600;
        super.background = MaterialColors.BLUE_700;
        c.setBackground(super.background);
        super.foreground = MaterialColors.WHITE;
        c.setForeground(super.foreground);

        if (super.mouseHoverEnabled) {
            c.addMouseListener(
                    MaterialUIMovement.getMovement(c, this.colorMouseHoverNormalButton)
            );
        }
        super.borderEnabled = false;
    }
}
