package GUI;

import Config.Config;
import DB.*;
import Exceptions.ProjectionNotSpecified;
import GUI.Agenda.Agenda;
import GUI.Theme.DangerContainedButtonUI;
import GUI.Theme.PrimaryContainedButtonUI;
import GUI.Theme.SecondaryContainedButtonUI;
import GUI.Types.ProjectionType;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ProjectionHandler extends JDialog {
    private JPanel contentPane;
    private JButton cancelButton;
    private JButton confirmButton;
    private JComboBox competitionComboBox;
    private JComboBox dayComboBox;
    private JComboBox slotComboBox;
    private JPanel competitionPanel;
    private JPanel slotPanel;
    private JPanel dayPanel;
    private JLabel competitionLabel;
    private JLabel slotLabel;
    private JLabel dayLabel;
    private JPanel filmPanel;
    private JLabel filmLabel;
    private JComboBox filmComboBox;
    private JPanel roomPanel;
    private JLabel roomLabel;
    private JComboBox roomComboBox;
    private ProjectionType projectionType;
    private Agenda agenda;
    private Projection projection = null;

    public ProjectionHandler(ProjectionType projectionType, Agenda agenda) throws ProjectionNotSpecified {
        if (projectionType == ProjectionType.EDIT)
            throw new ProjectionNotSpecified();
        this.projectionType = projectionType;
        this.agenda = agenda;
        $$$setupUI$$$();
        competitionComboBox.getModel().setSelectedItem(agenda.getCompetition());
        createUI();
        if (agenda.getDate() != null || agenda.getSlot() != null) {
            updateMovies();
            updateDates();
            if (IntStream.range(0, dayComboBox.getItemCount()).mapToObj(d -> dayComboBox.getItemAt(d)).filter(d -> d.equals(agenda.getDate())).toArray().length == 1)
                dayComboBox.getModel().setSelectedItem(agenda.getDate());
            updateSlots();
            if (IntStream.range(0, slotComboBox.getItemCount()).mapToObj(s -> slotComboBox.getItemAt(s)).filter(s -> s.equals(agenda.getSlot())).toArray().length == 1)
                slotComboBox.getModel().setSelectedItem(agenda.getSlot());
            updateRooms();
        }
    }

    public ProjectionHandler(ProjectionType projectionType, Agenda agenda, Projection projection) {
        this.projectionType = projectionType;
        this.agenda = agenda;
        this.projection = projection;
        $$$setupUI$$$();
        createUI();
        competitionComboBox.getModel().setSelectedItem(projection.getCompetition());
        filmComboBox.getModel().setSelectedItem(projection.getMovie());
        dayComboBox.getModel().setSelectedItem(projection.getStartDate().toLocalDate());
        slotComboBox.getModel().setSelectedItem(projection.getSlot());
        roomComboBox.getModel().setSelectedItem(projection.getRoom());
        if (projectionType == ProjectionType.REMOVE)
            for (JComboBox comboBox : Arrays.asList(competitionComboBox, filmComboBox, dayComboBox, slotComboBox, roomComboBox))
                comboBox.setEnabled(false);
    }

    private void createUI() {
        setContentPane(contentPane);
        setModal(true);
        setPreferredSize(new Dimension(500, 300));
        ImageIcon img = new ImageIcon("src/main/java/GUI/Assets/Logo/logo_cannes.jpg");
        setIconImage(img.getImage());
        setTitle(projectionType.getTitle());
        confirmButton.setText(projectionType.getText());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
        setLocationRelativeTo(null);
        updateCompetitions();
        competitionComboBox.addActionListener(actionEvent -> updateMovies());
        filmComboBox.addActionListener(actionEvent -> updateDates());
        dayComboBox.addActionListener(actionEvent -> updateSlots());
        slotComboBox.addActionListener(actionEvent -> updateRooms());
        cancelButton.addActionListener(actionEvent -> dispose());
        confirmButton.addActionListener(actionEvent -> confirm());
        contentPane.registerKeyboardAction(e -> dispose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onCancel() {
        dispose();
    }

    private void createUIComponents() {
        confirmButton = new JButton();
        if (projectionType == ProjectionType.REMOVE)
            confirmButton.setUI(new DangerContainedButtonUI());
        else
            confirmButton.setUI(new PrimaryContainedButtonUI());
        cancelButton = new JButton();
        cancelButton.setUI(new SecondaryContainedButtonUI());
    }

    private void updateCompetitions() {
        ArrayList<Competition> competitions = Competition.getAll();
        if (competitions != null)
            for (Competition c : competitions)
                competitionComboBox.addItem(c);
    }

    private void updateMovies() {
        filmComboBox.removeAllItems();
        dayComboBox.removeAllItems();
        slotComboBox.removeAllItems();
        roomComboBox.removeAllItems();
        ArrayList<Movie> movies = Movie.getAvailable((Competition) competitionComboBox.getSelectedItem());
        if (movies != null)
            for (Movie m : movies)
                filmComboBox.addItem(m);
    }

    private void updateDates() {
        dayComboBox.removeAllItems();
        slotComboBox.removeAllItems();
        roomComboBox.removeAllItems();
        Competition competition = (Competition) competitionComboBox.getSelectedItem();
        Movie movie = (Movie) filmComboBox.getSelectedItem();
        Set<Date> dates = competition.getProjections().stream().map(p -> p.getStartDate()).collect(Collectors.toSet());
        LocalDate[] localDates = Config.getStartDate().datesUntil(Config.getEndDate()).toArray(LocalDate[]::new);
        if (competition.getDays() != 0 && dates.size() >= competition.getDays() && !(projectionType == ProjectionType.EDIT && projection.getCompetition().getName().equals(competition.getName())))
            localDates = dates.stream().map(Date::toLocalDate).toArray(LocalDate[]::new);

        for (LocalDate date : localDates)
            if (movie == null || movie.validateDate(date))
                dayComboBox.addItem(date);
    }

    private void updateSlots() {
        slotComboBox.removeAllItems();
        roomComboBox.removeAllItems();
        if (dayComboBox.getSelectedItem() != null) {
            ArrayList<Slot> slots;
            Competition competition = (Competition) competitionComboBox.getSelectedItem();
            Date date = Date.valueOf((LocalDate) dayComboBox.getSelectedItem());
            if (projectionType == ProjectionType.REMOVE)
                slots = Slot.getAll(competition, date);
            else
                slots = Slot.getAvailable(competition, date);
            if (slots != null)
                for (Slot s : slots)
                    slotComboBox.addItem(s);
        }
    }

    private void updateRooms() {
        roomComboBox.removeAllItems();
        if (dayComboBox.getSelectedItem() != null) {
            ArrayList<Room> rooms;
            Movie movie = (Movie) filmComboBox.getSelectedItem();
            Slot slot = (Slot) slotComboBox.getSelectedItem();
            Date date = Date.valueOf((LocalDate) dayComboBox.getSelectedItem());
            if (projectionType == ProjectionType.REMOVE)
                rooms = Room.getAll(movie, slot, date);
            else
                rooms = Room.getAvailable(movie, slot, date);
            if (rooms != null)
                for (Room r : rooms)
                    roomComboBox.addItem(r);
        }
    }

    private void confirm() {
        for (JComboBox comboBox : Arrays.asList(competitionComboBox, filmComboBox, dayComboBox, slotComboBox, roomComboBox))
            if (comboBox.getSelectedItem() == null)
                return;
        Date date = Date.valueOf((LocalDate) dayComboBox.getSelectedItem());
        Competition competition = (Competition) competitionComboBox.getSelectedItem();
        Room room = (Room) roomComboBox.getSelectedItem();
        Movie movie = (Movie) filmComboBox.getSelectedItem();
        Slot slot = (Slot) slotComboBox.getSelectedItem();
        switch (projectionType) {
            case ADD:
                new Projection(date, competition, room, movie, slot);
                break;
            case EDIT:
                projection.delete();
                new Projection(date, competition, room, movie, slot);
                break;
            case REMOVE:
                if (projection != null)
                    projection.delete();
                else
                    (Projection.find(date, competition, room, movie, slot)).delete();
                break;
        }
        this.agenda.refresh();
        dispose();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(4, 2, new Insets(10, 10, 10, 10), -1, -1));
        competitionPanel = new JPanel();
        competitionPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(competitionPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        competitionLabel = new JLabel();
        competitionLabel.setText("Competition");
        competitionPanel.add(competitionLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_SOUTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        competitionComboBox = new JComboBox();
        competitionPanel.add(competitionComboBox, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        slotPanel = new JPanel();
        slotPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(slotPanel, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        slotLabel = new JLabel();
        slotLabel.setText("Slot");
        slotPanel.add(slotLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_SOUTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        slotComboBox = new JComboBox();
        slotPanel.add(slotComboBox, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dayPanel = new JPanel();
        dayPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(dayPanel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        dayLabel = new JLabel();
        dayLabel.setText("Day");
        dayPanel.add(dayLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_SOUTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        dayComboBox = new JComboBox();
        dayPanel.add(dayComboBox, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelButton.setText("Cancel");
        contentPane.add(cancelButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmButton.setText("Button");
        contentPane.add(confirmButton, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        filmPanel = new JPanel();
        filmPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(filmPanel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        filmLabel = new JLabel();
        filmLabel.setText("Film");
        filmPanel.add(filmLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_SOUTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        filmComboBox = new JComboBox();
        filmPanel.add(filmComboBox, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        roomPanel = new JPanel();
        roomPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(roomPanel, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        roomLabel = new JLabel();
        roomLabel.setText("Room");
        roomPanel.add(roomLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        roomComboBox = new JComboBox();
        roomPanel.add(roomComboBox, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
